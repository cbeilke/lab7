/**************************
 *Cliff Beilke 
 *CPSC 1021, Section 002, F20
 *cbeilke@g.clemson.edu
 *Nushrat Humaira and Evan Hastings
 **************************/

#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee
{
  string lastName;
  string firstName;
  int birthYear;
  double hourlyWage;
} employee;

bool name_order(const employee &lhs, const employee &rhs);
int myrandom(int i) { return rand() % i; }

int main(int argc, char const *argv[])
{
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned(time(0)));

  /*Create an array of 10 employees and fill information from standard input with prompt messages*/
  array<employee, 10> empArr; //c++ std style baby

  //take in user input
  for (int i = 0; i < (int)(sizeof(empArr) / sizeof(empArr[0])); i++)
  {

    cout << "Enter Employee's First Name: ";
    getline(cin, empArr[i].firstName); //getline for strings
    cout << "Enter Employee's Last Name: ";
    getline(cin, empArr[i].lastName);
    cout << "Enter Employee's Year of Birth: ";
    cin >> empArr[i].birthYear; //cin for ints
    cout << "Enter Employee's Hourly Wage: ";
    cin >> empArr[i].hourlyWage;
    cin.ignore(); //ignore anything after to make sure getline isnt skipped
  }

  //test code to print out input
  /*for (int i = 0; i < (sizeof(empArr) / sizeof(empArr[0])); i++)
  {
    cout << "Employee " << i + 1 << ": " << empArr[i].firstName << ", "
         << empArr[i].lastName << ", " << empArr[i].birthYear << ", "
         << empArr[i].hourlyWage << endl;
  }
  */

  /*After the array is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/
  //pass in the pointers that point to the begining, and one past the end
  random_shuffle(empArr.begin(), empArr.end(), myrandom);
  /*Build a smaller array of 5 employees from the first five cards of the array created
    *above*/

  //array initalizer for this
  array<employee, 5> smallEmpArr =
      {empArr[0], empArr[1], empArr[2], empArr[3], empArr[4]};

  /*Sort the new array.  Links to how to call this function is in the specs
     *provided*/
  //same pointers as before, plus the function made down below
  sort(smallEmpArr.begin(), smallEmpArr.end(), name_order);
  /*Now print the array below */
  //I thought it would look a lot cleaner to have each one on one line
  for (int i = 0; i < (int)(sizeof(smallEmpArr) / sizeof(smallEmpArr[0])); i++)
  {
    cout << "Employee " << i + 1 << ": " << smallEmpArr[i].firstName << ", "
         << smallEmpArr[i].lastName << ", " << smallEmpArr[i].birthYear << ", "
         << smallEmpArr[i].hourlyWage << endl;
  }

  return 0;
}

/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee &lhs, const employee &rhs)
{
  //IMPLEMENT
  //compares the strings together, returns false if left name is earlier in the alphabet
  //than right name, true if later (at least I think), tbh im pretty down with this
  //staying ambiguous
  return lhs.lastName < rhs.lastName;
}
